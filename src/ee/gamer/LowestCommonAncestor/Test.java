package ee.gamer.LowestCommonAncestor;

public class Test {

	private static Node root;
	static Node n14;
	static Node n18;
	static Node n12;
	static Node n6;

	public static void main(String[] args) {
		init();
		System.out.println(FindLCA.viaParents(n12, n6));
		System.out.println(FindLCA.viaRoot(root, n12, n6));

	}

	
	/*             0
	 *           /   \
	 *          2     3
	 *         / \     \
	 *        5   6     7
	 *       /         / \
	 *      11       14   15
	 *     /  \          / |\
	 *    12   13      16 17 18
	 */
	private static void init() {
		root = new Node(0);
		Node n2 = new Node(2);
		Node n5 = new Node(5);
		Node n11 = new Node(11);
		n12 = new Node(12);
		n11.addChild(n12);
		n11.addChild(new Node(13));
		n5.addChild(n11);
		n2.addChild(n5);
		n6 = new Node(6);
		n2.addChild(n6);
		root.addChild(n2);
		Node n3 = new Node(3);
		Node n7 = new Node(7);
		Node n15 = new Node(15);
		n14 = new Node(14);
		n18 = new Node(18);
		n7.addChild(n14);
		n7.addChild(n15);
		n3.addChild(n7);
		n15.addChild(new Node(16));
		n15.addChild(new Node(17));
		n15.addChild(n18);
		root.addChild(n3);
	}

}
