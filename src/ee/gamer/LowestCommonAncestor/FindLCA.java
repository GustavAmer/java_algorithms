package ee.gamer.LowestCommonAncestor;

import java.util.Set;

import javafx.util.Pair;

public class FindLCA {

	//worst case: O(n)
	public static Node viaParents(Node a, Node b) {
		Set<Node> path = LCAUtil.getPath(a); // LCAUtil.getPath(a, new HashSet<Node>());
		Node temp = b;		
		while (temp != null) {
			if (path.contains(temp))
				return temp;
			temp = temp.parent;
		}
		return null;
	}

	//Recursive, worst case: O(n)
	public static Node viaRoot(Node root, Node a, Node b) {
		return getLCA(root, a, b).getKey();
	}

	private static Pair<Node, Boolean> getLCA(Node root, Node a, Node b) {
		boolean self = root.equals(a) || root.equals(b);
		int count = 0;
		for (Node child : root.children) {
			Pair<Node, Boolean> childResult = getLCA(child, a, b);
			if (childResult.getValue().booleanValue())
				return childResult;
			if (childResult.getKey() != null) {
				count++;
			}
		}
		if ((self && count == 1) || count == 2) {
			return new Pair<Node, Boolean>(root, Boolean.TRUE);
		}
		if (count == 1 || self) {
			return new Pair<Node, Boolean>(root, Boolean.FALSE);
		}
		return new Pair<Node, Boolean>(null, Boolean.FALSE);
	}
}
