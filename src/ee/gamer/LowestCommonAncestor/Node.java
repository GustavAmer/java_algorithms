package ee.gamer.LowestCommonAncestor;

import java.util.ArrayList;
import java.util.List;


public class Node {

	public int value;
	public List<Node> children;
	public Node parent;

	public Node(int i) {
		value = i;
		children = new ArrayList<Node>();
	}
	
	public void addChild(Node n) {
		children.add(n);
		n.parent = this;
	}
	
	@Override
	public String toString() {
		return value+"";		
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Node && ((Node) o).value == value;
	}
}
