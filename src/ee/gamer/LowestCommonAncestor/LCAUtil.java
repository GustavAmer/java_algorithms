package ee.gamer.LowestCommonAncestor;

import java.util.HashSet;
import java.util.Set;

public class LCAUtil {

	public static Set<Node> getPath(Node node) {
		Set<Node> path = new HashSet<Node>(); // use HashSet for O(1) contains check
		Node temp = node;
		while (temp != null) {
			path.add(temp);
			temp = temp.parent;
		}
		return path;
	}

	// recursive
	public static Set<Node> getPath(Node node, Set<Node> path) {
		if (node == null) {
			return path;
		}
		path.add(node);
		return getPath(node.parent, path);
	}
}
